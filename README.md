# Oxide :shell:

[Rust](https://www.rust-lang.org) is a modern systems programming language focusing on safety, speed, and concurrency. It accomplishes these goals by being memory safe without using garbage collection.

This is a collection of runnable examples that illustrate various Rust concepts and standard libraries.
for more information please check out the [official docs](https://www.rust-lang.org/learn) and [Rust by Example (RBE)](https://doc.rust-lang.org/rust-by-example/)


## Requirements

 - [rustup](https://rustup.rs) to install Rust :wink:
 - [Rust](https://www.rust-lang.org) **1.44.1** and above
 - [Cargo](https://doc.rust-lang.org/cargo/) **1.44.1** and above


## Recommended tools
 - [rustfmt](https://github.com/rust-lang/rustfmt) **1.4.16-stable** and above
 - [cargo-make](https://github.com/sagiegurari/cargo-make) **0.31.0** and above
 - [clippy](https://github.com/rust-lang/rust-clippy) **0.0.212** and above


## Getting Started

### Clone the repo and make it yours:

```bash
$ git clone git@gitlab.com:HomeInside/Oxide.git
```

## Tutorials

You can run this examples with Rust **1.44.1** and above.

 - We are working on it... :smile:


## License

The full text of the licenses can be found in the MIT-LICENSE' file.
