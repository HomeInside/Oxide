/*
  A pure Rust implementation of Haversine formula
  Based on Bartek Górny implementation and others
  Author: Jorge Brunal Perez <diniremix@gmail.com>

  How to use:

  let origin: [f64; 2] = [51.5223337, -0.7198055];
  let destiny: [f64; 2] = [51.5226574, -0.7219567];

  let result_in_kms = haversine(origin, destiny, Some("kms"));
  println!("result in kms: {}", result_in_kms);

  Otherwise
  let result_in_mts = haversine(origin, destiny, Some("mts"));
  println!("result in mts: {}", result_in_mts);
*/

use std::f64::consts::PI;

fn haversine(orig: [f64; 2], dest: [f64; 2], type_measure: Option<&str>) -> f64 {
    const R: f64 = 6371f64;
    const RAD: f64 = PI / 180f64;
    const SECOND: f64 = 2f64;

    let orig_lat: f64 = orig[0];
    let orig_lon: f64 = orig[1];

    let dest_lat: f64 = dest[0];
    let dest_lon: f64 = dest[1];

    let delta_lat: f64 = (dest_lat - orig_lat) * RAD;
    let delta_lon: f64 = (dest_lon - orig_lon) * RAD;

    let rad_orig_lat: f64 = orig_lat * RAD;
    let rad_dest_lat: f64 = dest_lat * RAD;

    let a: f64 = (delta_lat / SECOND).sin().powi(2)
        + rad_orig_lat.cos() * rad_dest_lat.cos() * (delta_lon / SECOND).sin().powi(2);

    // 2 * asin(sqrt(a))
    let c: f64 = SECOND * a.sqrt().asin();
    let d: f64 = R * c;

    match type_measure {
        Some("kms") => {
            println!("type_measure results in kms!");
            format!("{:.3}", d).parse().unwrap()
        }
        _ => {
            println!("type_measure results in meters");
            format!("{:.3}", d * 1000f64).parse().unwrap()
        }
    }
}

fn main() {
    let origin: [f64; 2] = [51.5223337, -0.7198055];
    let destiny: [f64; 2] = [51.5226574, -0.7219567];

    let result_in_kms = haversine(origin, destiny, Some("kms"));
    println!("result in kms: {}", result_in_kms);

    let result_in_mts = haversine(origin, destiny, Some("mts"));
    println!("result in mts: {}", result_in_mts);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_haversine_1() {
        let origin: [f64; 2] = [51.5223337, -0.7198055];
        let destiny: [f64; 2] = [51.5226574, -0.7219567];

        let result_in_kms = haversine(origin, destiny, Some("kms"));
        assert_eq!(result_in_kms, 0.153);

        let result_in_mts = haversine(origin, destiny, Some("mts"));
        assert_eq!(result_in_mts, 153.124);
    }

    #[test]
    fn test_haversine_2() {
        let origin: [f64; 2] = [51.5146908, -0.0734275];
        let destiny: [f64; 2] = [51.5227776, -0.7220032];

        let result_in_kms = haversine(origin, destiny, Some("kms"));
        assert_eq!(result_in_kms, 44.885);

        let result_in_mts = haversine(origin, destiny, Some("mts"));
        assert_eq!(result_in_mts, 44885.119);
    }
}
