// many of these examples are from the official site of RBE, and used to learn Rust
// https://doc.rust-lang.org/rust-by-example/
// and used to learn Rust
//
// some others from blogs and references on the internet,
// and still others from my learning in this awesome language

fn match_factorial(i: u64) -> u64 {
    match i {
        0 => 1,
        n => n * match_factorial(n-1)
    }
}

fn recursive_factorial(n: i32) -> i32 {
    if n <= 1 {
        1
    } else {
        n * recursive_factorial(n-1)
    }
}

fn main() {
    println!("recursive factorial: {:?}", recursive_factorial(10));
    println!("match factorial: {:?}", match_factorial(10));
}
