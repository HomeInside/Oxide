// many of these examples are from the official site of RBE, and used to learn Rust
// https://doc.rust-lang.org/rust-by-example/
// and used to learn Rust
//
// some others from blogs and references on the internet,
// and still others from my learning in this awesome language

enum VeryVerboseEnumOfThingsToDoWithNumbers {
    Add,
    Subtract,
}

// Creates a type alias
type Operations = VeryVerboseEnumOfThingsToDoWithNumbers;

fn main() {
    // We can refer to each variant via its alias, not its long and inconvenient
    // name.
    let _x = Operations::Add;
    let _y = Operations::Subtract;

    //check this
    //Operations::Add => println!("Operations Add");
    //println!("Operations Add {:?}", _x);
    //println!("Operations Add {:#?}", _x);

    match _x {
		Operations::Add => println!("Operations x"),
		Operations::Subtract => println!("Operations y"),
    }
    println!("Operations end");
}
