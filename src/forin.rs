// many of these examples are from the official site of RBE, and used to learn Rust
// https://doc.rust-lang.org/rust-by-example/
// and used to learn Rust
//
// some others from blogs and references on the internet,
// and still others from my learning in this awesome language

fn inclusive_for(){
    // `n` will take the values: 1, 2, ..., 100 in each iteration
    for n in 1..=100 {
        if n % 15 == 0 {
            println!("fizzbuzz");
        } else if n % 3 == 0 {
            println!("fizz");
        } else if n % 5 == 0 {
            println!("buzz");
        } else {
            println!("{}", n);
        }
    }
}

fn normal_for(){
    // `n` will take the values: 1, 2, ..., 100 in each iteration
    for n in 1..101 {
        if n % 15 == 0 {
            println!("fizzbuzz");
        } else if n % 3 == 0 {
            println!("fizz");
        } else if n % 5 == 0 {
            println!("buzz");
        } else {
            println!("{}", n);
        }
    }
}

fn vector_for(){
    let names = vec!["Bob", "Frank", "Ferris"];

    for name in names.iter() {
        match name {
            &"Ferris" => println!("There is a rustacean among us!"),
            _ => println!("Hello {}", name),
        }
    }
}

fn into_iter_for(){
    let names = vec!["Bob", "Frank", "Ferris"];

    for name in names.into_iter() {
        match name {
            "Ferris" => println!("There is a rustacean among us!"),
            _ => println!("Hello {}", name),
        }
    }
}

fn iter_mut_for(){
    let mut names = vec!["Bob", "Frank", "Ferris"];

    for name in names.iter_mut() {
        *name = match name {
            &mut "Ferris" => "There is a rustacean among us!",
            _ => "Hello",
        }
    }

    println!("names: {:?}", names);
}

fn infinite_for(){
    let upper = 1000;
    // Iterate: 0, 1, 2, ... to infinity
    for n in 0.. {
        // Square the number
        let n_squared = n * n;

        println!("n: {}", n_squared);
        if n_squared >= upper {
            // Break loop if exceeded the upper limit
            break;
        }
    }
}

fn main() {
    inclusive_for();
    println!("\n");
    normal_for();
    println!("\n");
    vector_for();
    println!("\n");
    vector_for();
    println!("\n");
    into_iter_for();
    iter_mut_for();
    println!("\n");
    infinite_for();
}
