// many of these examples are from the official site of RBE, and used to learn Rust
// https://doc.rust-lang.org/rust-by-example/
// and used to learn Rust
//
// some others from blogs and references on the internet,
// and still others from my learning in this awesome language

// Our example enum
enum Foo {
    Bar,
    Baz
}

fn main() {
    // Create example variables
    let a = Foo::Bar;
    let b = Foo::Baz;

    // Variable a matches Foo::Bar
    if let Foo::Bar = a {
        println!("a is foobar");
    }

    // Variable b does not match Foo::Bar
    // So this will print nothing
    if let Foo::Baz = b {
        println!("b is foobaz");
    }
}
