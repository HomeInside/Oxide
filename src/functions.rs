// many of these examples are from the official site of RBE, and used to learn Rust
// https://doc.rust-lang.org/rust-by-example/
// and used to learn Rust
//
// some others from blogs and references on the internet,
// and still others from my learning in this awesome language

//define a single function
fn display_message(){
  println!("Hi, this is my user defined function");
}

// Arguments pass by Value
fn sum(mut n:i32){
  n = n * n;
  println!("The value of n inside sum function : {}", n);
}

// Arguments pass by Reference
fn square(x:&mut i32){
  *x = *x * *x;
  println!("The value of x inside function : {}", x);
}

fn main() {
	let n = 4;
	let mut m = 5;

	//invoke a single function
	display_message();

	println!();
	println!("The value of n before function call : {}", n);

	println!("Invoke Function sum...");
	sum(n);

	println!("\nThe value of n after function call : {}", n);
	println!();

	println!("The value of m before function call : {}", m);

	println!("Invoke Function square...");
	square(&mut m);

	println!("The value of m after function call : {}", m);
}
