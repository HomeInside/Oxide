// many of these examples are from the official site of RBE, and used to learn Rust
// https://doc.rust-lang.org/rust-by-example/
// and used to learn Rust
//
// some others from blogs and references on the internet,
// and still others from my learning in this awesome language

use std::thread;
use std::time::Duration;

fn main() {
    let handle = thread::spawn(|| {
        for i in 1..10 {
            println!("hi number {} from the spawned thread!", i);
            thread::sleep(Duration::from_millis(1));
        }
    });

    for i in 1..5 {
        println!("hi number {} from the main thread!", i);
        thread::sleep(Duration::from_millis(1));
    }
    handle.join();
    println!("finished");
}
