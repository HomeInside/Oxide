// many of these examples are from the official site of RBE, and used to learn Rust
// https://doc.rust-lang.org/rust-by-example/
// and used to learn Rust
//
// some others from blogs and references on the internet,
// and still others from my learning in this awesome language

fn main() {
    let mut a = 4;
    let b = 3;

    // Arithmetic Operators
    println!("Operand 1: {}, Operand 2: {}", a , b);
    println!();
    println!("Addition: {}", a + b);
    println!("Subtraction: {}", a - b);
    println!("Multiplication: {}", a * b);
    println!("Division: {}", a / b);
    println!("Modulus: {}", a % b);

    // Short-circuiting boolean logic
    println!("true AND false is {}", true && false);
    println!("true OR false is {}", true || false);
    println!("NOT true is {}", !true);

    // Comparison Operators
    println!("a > b: {}", a > b);
    println!("a < b: {}", a < b);
    println!("a >= b: {}", a >= b);
    println!("a <= b: {}", a <= b);
    println!("a == b: {}", a == b);
    println!("a != b: {}", a != b);

    // Bitwise operations
    println!("0011 AND 0101 is {:04b}", 0b0011u32 & 0b0101);
    println!("0011 OR 0101 is {:04b}", 0b0011u32 | 0b0101);
    println!("0011 XOR 0101 is {:04b}", 0b0011u32 ^ 0b0101);
    println!("1 << 5 is {}", 1u32 << 5);
    println!("0x80 >> 2 is 0x{:x}", 0x80u32 >> 2);

	// Assignment and Compound Assignment Operators
    //define a mutable variable
    a += 1;
    println!("a+=1: {}", a);

    a -= 1;
    println!("a-=1: {}", a);

    a /= 1;
    println!("a/=1: {}", a);

    a *= 3;
    println!("a/=3: {}", a);

    // Use underscores to improve readability!
    println!("One million is written as {}", 1_000_000u32);
}
