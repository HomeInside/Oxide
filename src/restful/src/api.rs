//! Api client for humans
//!
//! Tested with rust 1.45.0
//!
//! @author Jorge Brunal <https://gitlab.com/HomeInside>
//!
//! @license MIT license <http://www.opensource.org/licenses/mit-license.php>
//!

use serde_json::json;
use ureq::{Request, Response};

/// Api client for humans
pub struct Api {
    /// * `url_base` - un String con la url base
    url_base: String,
    /// * `bearer_token` - un Option String con el Bearer token
    bearer_token: Option<String>,
}

/// Impl para el manejo de la url base y el token
///
/// Permite crear una instancia de Api para su uso
impl Api {
    // static method
    /// crea una instancia de Api
    ///
    /// # Arguments
    ///
    /// * `base_url` - un String con la url base
    ///
    /// # Examples
    ///
    /// ```
    /// // Crea una instancia del Api
    /// let mut client = api::Api::new("http://awesome-url.com".to_string());
    /// ```
    pub fn new(base_url: String) -> Api {
        Api {
            url_base: base_url,
            bearer_token: None,
        }
    }

    /// fija un token de tipo Bearer
    ///
    /// # Arguments
    ///
    /// * `token` - un String con el token de tipo Bearer
    ///
    /// # Examples
    ///
    /// ```
    /// let token = String::from("awesomeToken");
    /// client.set_token(token);
    /// ```
    pub fn set_token(&mut self, token: String) {
        self.bearer_token = Some(token);
    }

    /// remueve un Bearer token existente
    ///
    /// # Examples
    ///
    /// ```
    /// client.remove_token();
    /// ```
    pub fn remove_token(&mut self) {
        self.bearer_token = None;
    }

    /// fija el Content-Type y el Bearer Token a una peticion
    ///
    /// # Arguments
    ///
    /// * `request` - un objeto tipo ureq::Request
    ///
    /// # Examples
    ///
    /// ```
    /// let mut request = agent.get(&uri);
    /// request = self.set_auth_headers(request);
    /// ```
    fn set_auth_headers(&mut self, mut request: Request) -> Request {
        request.set("Content-Type", "application/json");

        match &self.bearer_token {
            Some(token_value) => {
                let token = format!("Bearer {}", token_value);
                request.set("Authorization", &token);
            }
            None => println!("bearer_token is None"),
        }

        request
    }

    /// construye una respuesta basada en el servicio solicitado
    ///
    /// # Arguments
    ///
    /// * `result` - un objeto tipo ureq::Response
    ///
    /// # Examples
    ///
    /// ```
    /// let result = request.call();
    /// return self.make_response(result)
    /// ```
    fn make_response(
        &mut self,
        result: Response,
    ) -> Result<serde_json::value::Value, serde_json::value::Value> {
        if result.ok() {
            let resp = result.into_json().unwrap();

            Ok(resp)
        } else if result.client_error() {
            println!("⚠️ client_error {:?}", result);

            Err(json!(result.into_json().unwrap()))
        } else if result.server_error() {
            println!("⚠️ server_error {:?}", result);

            Err(json!({
                "status": result.status(),
                "message": result.status_text(),
                "data": "infernal server error",
            }))
        } else {
            println!("⚠️ FAIL {:?}", result);

            Err(json!({
                "status": result.status(),
                "message": result.status_text(),
                "data": "infernal server error",
            }))
        }
    }

    /// Retorna un Result con un JSON del servicio solicitado
    ///
    /// # Arguments
    ///
    /// * `end_point` - un string slice con el nombre del endpoint
    ///
    /// # Examples
    ///
    /// ```
    /// // invoca al metodo get
    /// let result = client.get("/get");
    /// ```
    pub fn get(
        &mut self,
        end_point: &str,
    ) -> Result<serde_json::value::Value, serde_json::value::Value> {
        let uri = format!("{}{}", self.url_base, end_point);
        let agent = ureq::agent();
        let mut request = agent.get(&uri);

        request = self.set_auth_headers(request);
        let result = request.call();

        self.make_response(result)
    }

    /// Retorna un Result con un JSON del servicio solicitado
    ///
    /// # Arguments
    ///
    /// * `end_point` - un string slice con el nombre del endpoint
    /// * `data` - un `serde::json!` con la data a enviar
    ///
    /// # Examples
    ///
    /// ```
    /// // invoca al metodo post
    /// let result = client.post("/post", &data);
    /// ```
    pub fn post(
        &mut self,
        end_point: &str,
        data: &serde_json::value::Value,
    ) -> Result<serde_json::value::Value, serde_json::value::Value> {
        let uri = format!("{}{}", self.url_base, end_point);
        let agent = ureq::agent();
        let mut request = agent.post(&uri);

        request = self.set_auth_headers(request);
        let result = request.send_json(json!(data));

        self.make_response(result)
    }

    /// Retorna un Result con un JSON del servicio solicitado
    ///
    /// # Arguments
    ///
    /// * `end_point` - un string slice con el nombre del endpoint
    /// * `data` - un `serde::json!` con la data a modificar
    ///
    /// # Examples
    ///
    /// ```
    /// // invoca al metodo put
    /// let result = client.post("/put", &data);
    /// ```
    pub fn put(
        &mut self,
        end_point: &str,
        data: &serde_json::value::Value,
    ) -> Result<serde_json::value::Value, serde_json::value::Value> {
        let uri = format!("{}{}", self.url_base, end_point);
        let agent = ureq::agent();
        let mut request = agent.put(&uri);

        request = self.set_auth_headers(request);
        let result = request.send_json(json!(data));

        self.make_response(result)
    }

    /// Retorna un Result con un JSON del servicio solicitado
    ///
    /// # Arguments
    ///
    /// * `end_point` - un string slice con el nombre del endpoint
    ///
    /// # Examples
    ///
    /// ```
    /// // invoca al metodo delete
    /// let result = client.delete("/delete");
    /// ```
    pub fn delete(
        &mut self,
        end_point: &str,
    ) -> Result<serde_json::value::Value, serde_json::value::Value> {
        let uri = format!("{}{}", self.url_base, end_point);
        let agent = ureq::agent();
        let mut request = agent.delete(&uri);

        request = self.set_auth_headers(request);
        let result = request.call();

        self.make_response(result)
    }
}
