extern crate serde;
extern crate serde_json;

mod api;
use api::Api;

fn main() {
    println!("Hello, RestFul world!");

    // Allows you to create an Api instance for your use
    let mut client = Api::new(String::from("https://httpbin.org"));

    // a `serde_json::json!` with sample data
    let data = serde_json::json!({
        "name": "Ferris",
        "lastname":"the crab",
        "rustacean": true
    });

    // send a sample token
    let token = String::from("awesome-token-bearer");
    client.set_token(token);

    // make a GET request
    let result = client.get("/get");
    if result.is_ok() {
        println!("POST success: {:?}", result);
    } else {
        println!("POST error: {:?}", result);
    }

    println!();
    // remove the previously established Token
    client.remove_token();

    // make a POST request
    let result = client.post("/post", &data);
    if result.is_ok() {
        println!("POST success: {:?}", result);
    } else {
        println!("POST error: {:?}", result);
    }

    println!();
    // send a sample token
    let token = String::from("another-token-bearer");
    client.set_token(token);

    // make a PUT request
    let result = client.put("/put", &data);
    if result.is_ok() {
        println!("PUT success: {:?}", result);
    } else {
        println!("PUT error: {:?}", result);
    }

    println!();
    // remove the previously established Token
    client.remove_token();

    // make a DELETE request
    let result = client.delete("/delete");
    if result.is_ok() {
        println!("DELETE success: {:?}", result);
    } else {
        println!("DELETE error: {:?}", result);
    }
}
